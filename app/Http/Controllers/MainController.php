<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use App\Models\User;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function home() {
        return view('home');
    }


    public function review() {
        $reviews = new Contact();
        return view('review', ['reviews'=>$reviews->all()]);
    }

    public function review_check(Request $request) {
        $valid = $request->validate([
            'email'=>'required|min:4|max:50',
            'subject'=>'required|min:4|max:50',
            'message'=>'required|min:15|max:500',

        ]);

        $review = new Contact();
        $review->email = $request->input('email');
        $review->subject = $request->input('subject');
        $review->message = $request->input('message');

        $review->save();
        return redirect('review');
    }


    public function review_delete($id) {
        Contact::destroy($id);
        return redirect('review');
    }
}
