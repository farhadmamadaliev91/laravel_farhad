@extends('layout')

@section('title')
    Todo list
@endsection

@section('main_content')
        <h1>To do app</h1>
        @if($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
       @endif
    <form method="post" action="/review/check">
        @csrf
        <input type="email" name="email" id="email" placeholder="Введите email" class="form-control"><br>
        <input type="text" name="subject" id="subject" placeholder="Введите название задачи" class="form-control"><br>
        <textarea name="message" id="message" cols="30" rows="10" class="form-control" placeholder="Введите комментарии к задачи" ></textarea><br>
        <button type="submit" class="btn btn-success">Добавить</button>
    </form>
    <br>
   <h1> Все задачи</h1>
    @foreach($reviews as $el)
        <div class="alert alert-warning">
        <h3>{{$el->subject}}</h3>
        <p>{{$el->message}}</p>


            <form action="{{url('review/'. $el->id)}}" method="POST">

                {{csrf_field()}}
                <button type="submit" class="btn btn-outline-danger">

                    <span class="icon is-small"> <i class="fa fa-btn fa-trash"></i></span>
                    <span> Удалить </span>
                </button>

            </form>

    </div>
    @endforeach


@endsection
